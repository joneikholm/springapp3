package com.eikholm.dat17v3.springapp3;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController
{
    List<User> users = new ArrayList<>();

    @RequestMapping(value = { "/", "/index"}, method = RequestMethod.GET)
    public String index(){
        System.out.println("received GET request");
        return "index";
    }


    @RequestMapping(value = { "/", "/index"}, method = RequestMethod.POST)
    public String indexLogin(@ModelAttribute User user, Model model){
        users.add(user);
        model.addAttribute("users", users);
        model.addAttribute("user", user);
        System.out.println("received POST request " + user.getUsername());
        return "login";
    }

}
